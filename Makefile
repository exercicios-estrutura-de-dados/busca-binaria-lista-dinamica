# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: listaDinamicaDescritor.o main.o
	$(CC) $(CFLAGS) main.o include/listaDinamicaDescritor.o -o main

main.o: main.c
	$(CC) $(CFLAGS) -c main.c -o main.o

listaDinamicaDescritor.o: include/listaDinamicaDescritor.c include/listaDinamicaDescritor.h
	$(CC) $(CFLAGS) -c include/listaDinamicaDescritor.c -o include/listaDinamicaDescritor.o

clean:
	rm *.o include/*.o main

run:
	./main

debug:
	make CFLAGS+=-g
