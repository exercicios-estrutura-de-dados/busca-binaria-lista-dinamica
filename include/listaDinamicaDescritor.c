#include "listaDinamicaDescritor.h"
#include <stdlib.h>

// Cria a lista
LISTA* criar_lista(){
    LISTA* lista = (LISTA*)malloc(sizeof(LISTA));
    if(lista != NULL){
        lista->inicio = NULL;
        lista->final = NULL;
        lista->qtd = 0;
    }
    return lista;
}

// Verifica se a lista está vazia
int lista_vazia(LISTA* lista){
    if(lista == NULL || lista->qtd == 0){
        return 1;
    }else{
        return 0;
    }
}

// Insere na lista de forma ordenada
int inserir_lista(LISTA* lista, int numero){
    if(lista == NULL) return 0; // Lista não existe

    ELEM* elemento = (ELEM*)malloc(sizeof(ELEM));
    if(elemento == NULL) return 0; // Não foi possível alocar espaço para o elemento
    elemento->numero = numero;
    elemento->proximo = NULL;

    if(lista_vazia(lista)){ // Inserção do primeiro elemento da lista
        lista->inicio = elemento;
        lista->final = elemento;
    }else{ // Procura onde inserir
        ELEM *noAnterior, *noAuxiliar = lista->inicio;
        while( (noAuxiliar != NULL) && (noAuxiliar->numero < numero) ){ // Para quando noAuxiliar->numero >= numero
            noAnterior = noAuxiliar;
            noAuxiliar = noAuxiliar->proximo;
        }
        // Verifica resultado da busca
        if( (noAuxiliar != NULL) && (noAuxiliar->numero == numero) ){
            return -1; // Número já está na lista
        }
        if(noAuxiliar == lista->inicio){ // Inserção no início: noAuxiliar → noSeguinte ...
            lista->inicio = elemento; // elemento vira início da lista
            elemento->proximo = noAuxiliar; // elemento → noAuxiliar → noSeguinte ...
        }
        else{ // Outros casos, inserção no meio ou final: ... noAnterior → noAuxiliar
            noAnterior->proximo = elemento;  // noAnterior  → elemento
            elemento->proximo = noAuxiliar;  // noAnterior → elemento → noAuxiliar
            if(noAuxiliar == NULL){ // Inserção no final
                lista->final = elemento; // elemento vira final da lista
            }
        }
    }
    lista->qtd++; // Incrementa quantidade de elementos

    return 1;
}

// Busca elemento na lista
int busca_binaria(LISTA* lista, int numero){
    if(lista_vazia(lista)) return 0;

    ELEM *inicio = lista->inicio;
    int quantidade = lista->qtd;

    while(quantidade >= 1){
        int meio_lista = (quantidade) / 2;
        ELEM *noAuxiliar = inicio;

        if( (noAuxiliar == NULL) || ( quantidade<=1  &&  noAuxiliar->numero!=numero) ){ // Sobrou 1 elemento e não é o número procurado, ou é NULL
            return 0; // Não encontrou o número
        }

        // Vai até meio da lista
        for(int i=1; (noAuxiliar != NULL) && i < meio_lista; i++){
            noAuxiliar = noAuxiliar->proximo;
        }
                                                    // [ inicio_lista/esquerda ] < [ meio_lista ] < [ direita/fim_lista ]
        if(numero == noAuxiliar->numero){           // → Número está no meio da lista?
            return 1;                               //      então encontrou o número
        }else if(numero < noAuxiliar->numero){      // → Número está à esquerda de "meio_lista"?
            quantidade -= (meio_lista + 1);         //      então ignora a metade da direita
        }else{                                      // → Se não, número está à direita de "meio_lista"
            inicio = noAuxiliar->proximo;           //      então ignora a metade esquerda
            quantidade -= meio_lista;
        }
    }

    return 0; // Não encontrou o número
}

// Retorna um vetor com os números da lista
int* obter_lista(LISTA* lista, int* tamanho_vetor){
    if(lista_vazia(lista)) return 0;

    int *numero = calloc(lista->qtd, sizeof(int)); // Aloca vetor na memória
    if(numero != NULL){ // Verifica se alocação foi bem sucedida
        ELEM *noAuxiliar = lista->inicio;
        for(int i=0; (noAuxiliar != NULL) && i < (lista->qtd); i++){
            numero[i] = noAuxiliar->numero; // Semeia o vetor com os números
            noAuxiliar = noAuxiliar->proximo;
        }
        *tamanho_vetor = lista->qtd; // Informa o tamanho do vetor
        return numero;
    }else{
        *tamanho_vetor = 0;
        return NULL;
    }
}
