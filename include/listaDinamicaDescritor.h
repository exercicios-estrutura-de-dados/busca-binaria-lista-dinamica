#ifndef _LISTA_DINAMICA_COM_NO_DESCRITOR_H_
#define _LISTA_DINAMICA_COM_NO_DESCRITOR_H_

/*************** tipos de dados da lista ***********/

struct elemento_da_lista{ // Define como é um elemento(nó) desta lista
    int numero; // Guarda um número
    struct elemento_da_lista *proximo; // Ponteiro para o próximo elemento da lista
}typedef ELEM;

struct no_descritor{ // Nó descritor com as informações da lista
    ELEM* inicio;
    ELEM* final;
    int qtd;
}typedef LISTA; // "Lista" é um nó descritor com as informações da lista


/*************** Funções da lista **************/

// Cria a lista
LISTA* criar_lista();

// Verifica se a lista está vazia
int lista_vazia(LISTA* lista);

// Insere na lista de forma ordenada
int inserir_lista(LISTA* lista, int numero);

// Busca elemento na lista
int busca_binaria(LISTA* lista, int numero);

// Retorna um vetor com os números da lista
int* obter_lista(LISTA* lista, int* tamanho_vetor);

#endif
