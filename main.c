#include "include/listaDinamicaDescritor.h"
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
int primeiraExecucao=1, codigoRetornado;
LISTA* lista;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    #include <windows.h>
#endif

void limparTela(){
    printf("\033[H\033[2J\033[3J");
}
void clearStdinBuffer(){ // Limpa o buffer de stdIn
    while(getchar() != '\n');
}
int main(){
    int numero, *numerosGuardados=NULL, tamanho_vetor;
    unsigned int opcaoEscolhida;

    if(primeiraExecucao){
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
            SetConsoleCP(65001);  // Ativa UTF-8 como CodePage
	        SetConsoleOutputCP(65001);  // Ativa UTF-8 como CodePage de saída
	        setlocale(LC_ALL, "pt_BR.UTF-8");
        #else
            setlocale(LC_ALL,"Portuguese");
        #endif
        primeiraExecucao = 0;
        lista = criar_lista();
    }

    printf(
        "\x1B[1;32m"
        "======== Menu principal ==========\x1B[0m\n"
        "1 - inserir elemento \n"
        "2 - buscar elemento \n"
        "3 - listar elementos \n"
        "0 - sair \n"
        ">: "
    );
    opcaoEscolhida=13;
    scanf("%1u", &opcaoEscolhida);
    clearStdinBuffer();

    switch(opcaoEscolhida){
        case 1:
            limparTela();
            printf(
                "\x1B[1;32m"
                "===== inserir elemento =====\x1B[0m\n"
                "Número: "
            );
            scanf("%d", &numero);
            clearStdinBuffer();

            codigoRetornado = inserir_lista(lista, numero);

            limparTela();
            if(codigoRetornado == 1){
                printf("Número inserido com sucesso \n");
            }else if(codigoRetornado == -1){
                printf("Número já está na lista \n");
            }else{
                printf("Erro ao inserir número \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 2:
            limparTela();
            printf(
                "\x1B[1;32m"
                "===== buscar elementos =====\x1B[0m\n"
                "Número: "
            );
            scanf("%d", &numero);
            clearStdinBuffer();

            codigoRetornado = busca_binaria(lista, numero);

            limparTela();
            if(codigoRetornado == 1){
                printf("Número %d foi encontrado \n", numero);
            }else{
                printf("Número não encontrado \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 3:
            limparTela();
            numerosGuardados = obter_lista(lista, &tamanho_vetor);

            if((numerosGuardados != NULL) && (tamanho_vetor > 0)){
                printf(
                    "Elementos encontrados na lista\n"
                    ">: "
                );
                for(int i=0; i < tamanho_vetor; i++){
                    printf("%d", numerosGuardados[i]);
                    
                    (i == tamanho_vetor-1) ? printf("\n") : printf(", ");
                }
                free(numerosGuardados); // Libera vetor da memória
            }else{
                printf("Não foram encontrados elementos \n");
            }
            main();
            break;
        case 0:
            limparTela();
            printf("Saindo do programa... \n");
            break; // Finaliza o programa
        default:
            limparTela();
            printf("Opção inválida! Escolha novamente \n");
            main(); // Volta para main e mostra menu
    };
    return 0;
}
